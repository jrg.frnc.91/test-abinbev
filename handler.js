'use strict';

const AWS = require('aws-sdk');
const Sequelize = require('sequelize');
const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

// The code below IS NOT tested (I don't have access to an AWS account right now)
// The idea is to get the data source and some details to query it (as generic as possible)
module.exports.getDataFrom = async event => {
  const data = JSON.parse(event.body);
  switch(data.dataSource.name) {
    case 'dynamo': 
      // dataSource.query MUST be a JSON
      const result = await ddb.query(JSON.parse(data.dataSource.query)).promise();
      // Do something with the result...
      break;
    case 'rdms':
      // Use an ORM to help abstract the details to access our RDMS
      const sequelize = new Sequelize(data.dataSource.connString);
      // dataSource.query MUST be a string
      const [results, metadata] = await sequelize.query(data.dataSource.query);
      // Do something with the result...
      break;
    default:
      // If the data source is not dynamo db nor a rdms then there is a problem :(
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: `Fetched data from ${event.dataSource.name}...`
    }, null, 2),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

